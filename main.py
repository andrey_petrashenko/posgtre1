import psycopg2  # PostgreSQL library!
from psycopg2.extras import DictCursor

# connect to db
conn = psycopg2.connect(dbname='postgres', user='postgres',
                        password='1', host='localhost')

# create cursor
cursor = conn.cursor(cursor_factory=DictCursor)

# run a SQL query
cursor.execute('select * from public."Student"')
records = cursor.fetchall()
for record in records:
    print(record['name'])

# print([record['name'][:3] for record in records])
